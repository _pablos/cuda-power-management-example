#ifndef _UTIL_CPP
#define _UTIL_CPP

#include <cstdlib>

#include "util.h"

void util::create_random_vector(std::vector<float>& vector, unsigned length)
{
    if (vector.size() != length) vector.resize(length);
    
    for (unsigned i=0; i < length; ++i)
    {
	vector[i] = ((rand() % 1000)/1000.0);
    }
}

#endif // #ifndef _UTIL_CPP