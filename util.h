#ifndef _UTIL_H
#define _UTIL_H

#include <iostream>
#include <vector>

namespace util
{
    /**
     * @brief Fills an empty vector with random numbers.
     * 
     * @param vector An empty vector to fill.
     * @param length The length the vector must have.
     * @return void
     */
    void create_random_vector(std::vector<float>& vector, unsigned int length);

    /**
     * @brief Shows the elements of a vector
     * 
     * @param vector Vector to show
     * @return void
     */
    template<typename T>
    void print_vector(const std::vector<T>& vector)
    {
	typename std::vector<T>::const_iterator it;
	for (it = vector.begin(); it != vector.end(); ++it)
	{
	    std::cout << *it << "\t";
	}
	std::cout << std::endl;
    }
}

#endif //_UTIL_H