#include "power_manager.h"

Power_manager::Power_manager(unsigned device_index = 0)
{
    nvmlInit();
    nvmlDeviceGetHandleByIndex(device_index, &device);
}

Power_manager::~Power_manager()
{
    nvmlShutdown();
}

unsigned Power_manager::power_usage()
{
    unsigned milli_watts = 0;

    nvmlDeviceGetPowerUsage(device, &milli_watts);
    return milli_watts;
}
