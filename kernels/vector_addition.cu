#ifndef _VECTOR_ADDITION_KERNEL_CU
#define _VECTOR_ADDITION_KERNEL_CU

#include <cuda.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdexcept>

#include "kernels.h"

/*
 * 
 */
__global__ 
void vector_addition_kernel(float * result, float * vec1, float * vec2, int lenght)
{
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    
    if (i < lenght)
    {
	result[i] = vec1[i] + vec2[i];
    }
}

/*
 * 
 */
__host__
void kernel::vector_addition(float * result, const float * vec1, const float * vec2, unsigned vector_length)
{
    float * d_result = 0;
    float * d_vec1   = 0;
    float * d_vec2   = 0;
    const float * h_vec1  = vec1;
    const float * h_vec2  = vec2;
    
    cudaMalloc((void**)&d_result, vector_length * sizeof(float));
    cudaMalloc((void**)&d_vec1,   vector_length * sizeof(float));
    cudaMalloc((void**)&d_vec2,   vector_length * sizeof(float));
    //printf("cudaMalloc error: %s\n", cudaGetErrorString(cudaGetLastError()));
    
    cudaMemcpy(d_vec1, h_vec1, vector_length * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_vec2, h_vec2, vector_length * sizeof(float), cudaMemcpyHostToDevice);
    //printf("cudaMemcpy error: %s\n", cudaGetErrorString(cudaGetLastError()));
    
    int threads = 256;
    int blocks  = (vector_length + threads - 1) / threads;
    vector_addition_kernel <<< blocks, threads >>>(d_result, d_vec1, d_vec2, vector_length);
    //printf("kernel error: %s\n", cudaGetErrorString(cudaGetLastError()));

    cudaMemcpy(result, d_result, vector_length * sizeof(float), cudaMemcpyDeviceToHost);
      
    cudaFree(d_vec1);
    cudaFree(d_vec2);
}

/*
 *
 */
__host__
void kernel::vector_addition(std::vector<float>& result, const std::vector<float>& vec1, const std::vector<float>& vec2)
{
    if ((result.size() != vec1.size()) || 
        (result.size() != vec2.size()))
        throw std::runtime_error("Vectors have different sizes.");        
    
    kernel::vector_addition(result.data(), vec1.data(), vec2.data(), result.size());
}

#endif // #ifndef _VECTOR_ADDITION_KERNEL_CU
