#ifndef _KERNELS_H
#define _KERNELS_H

#include <vector>

namespace kernel
{

    void vector_addition(float * result, const float * vec1, const float * vec2, unsigned vector_length);
    void vector_addition(std::vector<float>& result, const std::vector<float>& vec1, const std::vector<float>& vec2);
    //float vector_reduction(const std::vector<float> & data);

}
   
   
#endif //_VECTOR_REDUCTION_H