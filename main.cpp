#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>

#include "kernels/kernels.h"
#include "power_manager.h"
#include "util.h"

int main(int argc, char **argv) 
{
    const int VECTOR_LENGTH = 256;
       
    Power_manager power_manager;

    std::vector<float> vector1;
    std::vector<float> vector2;
    std::vector<float> result;
    
    util::create_random_vector(vector1, VECTOR_LENGTH);
    util::create_random_vector(vector2, VECTOR_LENGTH);
    result.resize(VECTOR_LENGTH);
    
    kernel::vector_addition(result, vector1, vector2);
    std::cout << power_manager.power_usage() << std::endl;
    
    util::print_vector<float>(vector1);
    util::print_vector<float>(vector2);
    util::print_vector<float>(result);
    
    return 0;
}
