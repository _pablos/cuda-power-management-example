#ifndef _POWER_MANAGER_H
#define _POWER_MANAGER_H

#include <nvml.h>

class Power_manager
{
public:
    Power_manager();
    ~Power_manager();
    unsigned power_usage();

private:
    nvmlDevice_t device;
};

#endif //_POWER_MANAGER_H
